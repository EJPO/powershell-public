#Downloads Windows System Internals from the Internet and extracts into C:\sys
#I found I was doing this to many times so I automated it then improved the script.
#You can use this download and extract a zip file pretty much wherever you have permission to.
#Tested on PoSh 5.1
#Licence: BSD 2-Clause Licence
#Copyright (c) 2018, Ethan O'Donnell

#@TODO: Check if powershell is running in constrained mode before using .NET WebClient. Use .NET webclient if either BITS/Invoke-WebRequest are unavailable/error out.
#Seperate the Extract and Download into seperate functions so code can be eailsy resused.

#Variables
$ArchiveURL = "https://download.sysinternals.com/files/SysinternalsSuite.zip"
$DestinationFolder = "C:\sys"
$base64alphabet = "a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","1","2","3","4","5","6","7","8","9","0"
$ProgressPreferenceBackup = $ProgressPreference

#Functions
function Generate-Random($length){
    $loopvar = 0
    $randomstring = ""
    while ($loopvar -lt $length){
        [int]$charnum = get-random -Minimum 0 -Maximum 61
        $randomstring += $base64alphabet[$charnum]
        $loopvar ++
    }
    $randomstring
}

function DownloadAndUnpackArchive($ArchiveURI,$Folder){
    #Stops Transfers being tied to UI thread hence speeding them up.
    $ProgressPreference = "SilentlyContinue"
    $Private:Filename = Split-Path $ArchiveURI -Leaf

    [string]$tempfilelocation = $env:TEMP + "\" + "$(Generate-Random 8)" + "\$filename"
    New-Item -ItemType Directory -Path $tempfilelocation

    if((Test-Path $Folder) -eq $false){
        New-Item -Type Directory -Path $Folder
    }
    
   if(($PSVersionTable.PSVersion.Major) -le 3){
        #If Powershell Version is V3 or Below
        Write-Information "Powershell V3 or below detected. Using the .NET WebClient"
        $Private:WebClient = New-Object System.Net.WebClient
        $Private:WebClient.DownloadFile($ArchiveURI,$tempfilelocation)
    }elseif((get-service BITS).Status -eq "Running" -and (Get-Command Start-BitsTransfer -ErrorAction "SilentlyContinue")){
        #If BITS is running and the Module is available
        Write-Information "BITS is running. Using BITS to download the file"
        Start-BitsTransfer -Source $ArchiveURI -Destination $tempfilelocation
        break
    }elseif(Get-Command -Name "Invoke-WebRequest" -ErrorAction "SilentlyContinue"){
        #If all else fails, fall back to Invoke-WebRequest if it is available
        Write-Information "BITS is not running or the Module is not available. Falling back to Invoke-Webrequest"
        Invoke-WebRequest -Uri $ArchiveURI -OutFile $tempfilelocation
    }else{
        #No Methods to download the file could be used.
        Write-Error "No suported method to download file exists. Are you running powershell in constrained mode?"
        Remove-Item -Path $filelocation -Recurse -Force:$true -Confirm:$false
        $ProgressPreference = $ProgressPreferenceBackup
        exit
    }

    #Extract Archive to Final Path
    Expand-Archive -Path "$tempfilelocation" -DestinationPath $Folder
    Write-Information "Archive Extracted"

    #Delete Temporary Folder
    Remove-Item -Path $filelocation -Recurse -Force:$true -Confirm:$false
    $ProgressPreference = $ProgressPreferenceBackup
}

try {
    DownloadAndUnpackArchive $ArchiveURL $DestinationFolder
}
catch {
    #Store Error in Variable for no paticular reason
    $ProgressPreference = $ProgressPreferenceBackup
    $ErrorMessage = $_.Exception.Message
    if($ErrorMessage -eq $null){
        Write-Error "An unhandled exception has occured! Exiting for Safety"
    }else{
        Write-Error $ErrorMessage
    }
    Remove-Item -Path $filelocation -Recurse -Force:$true -Confirm:$false
    exit
}
